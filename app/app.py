from flask import Flask,redirect, url_for, Response
from flask import request
from model import db
from model import User
from model import CreateDB
from model import app as application
import simplejson as json
from sqlalchemy.exc import IntegrityError
import pickle
import os
import httplib
from threading import Thread
from flask import jsonify

# initate flask app
app = Flask(__name__)


#Get user details using expense id
@app.route('/v1/expenses/<expense_id>', methods = ['GET'])
def set(expense_id):
	try:
		db.create_all()
		rows= User.query.filter_by(id=expense_id).all()
		get_users = {}
		for expid in rows:
			get_users = {
							'id': expid.id,
							'name': expid.name,
							'email': expid.email,
							'category' : expid.category,
							'description' : expid.description,
							'link' : expid.link,
							'estimated_costs' : expid.estimated_costs,
							'submit_date' : expid.submit_date,
							'status' : expid.status,
							'decision_date' : expid.decision_date
						    }

		if get_users == {}:
    			return Response(status=httplib.NOT_FOUND)
		else:
			resp = jsonify(get_users)
			resp.status_code =200
			return resp
		
	except Exception,e:
		return str(e)


#post a new user details
@app.route('/v1/expenses',methods= ['POST'] )
def setup1():
	try:
		db.create_all()
		g = request.get_json(force=True) 
		name = g['name']
		email = g['email']
		category = g['category']
		description = g['description']
		link = g['link']
		estimated_costs = g['estimated_costs']
		submit_date = g['submit_date']
		status = "pending"
		decision_date = ""
		expenses = User(name,email,category,description,link,estimated_costs,submit_date,status,decision_date)

		db.session.add(expenses)
		db.session.commit()

		idee = User.query.filter_by(name=name).first().id
		di = {'id':idee,'name':name,'email':email,'category':category,'description':description,'link':link,'estimated_costs':estimated_costs,
		'submit_date':submit_date,'status':status,'decision_date':decision_date}

		val = jsonify(di)
		val.status_code = 201
		return val

	except Exception,e:
		db.session.rollback()
		return str(e)

#update the estimated_costs of a particular expense id
@app.route('/v1/expenses/<expense_id>', methods = ['PUT'])
def setup3(expense_id):
	try:
		g =  request.get_json(force=True)
		est_cost = g['estimated_costs']
		update = db.session.query(User).filter_by(id=expense_id).update({"estimated_costs":est_cost})
		db.session.commit()
		return Response(status=httplib.ACCEPTED)

	except Exception,e:
		return str(e)


#delete user records
@app.route('/v1/expenses/<expense_id>', methods = ['DELETE'])
def setup4(expense_id):
	try:
		delete = User.query.filter_by(id=expense_id).first()
		db.session.delete(delete)
		db.session.commit()
		return Response(status=httplib.NO_CONTENT)

	except:
		return "Delete Error"
	
#get the db info	
@app.route('/info')
def app_status():
	return json.dumps({'server_info':application.config['SQLALCHEMY_DATABASE_URI']})

# run app service 
if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5000, debug=True)

